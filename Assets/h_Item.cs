﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class h_Item : MonoBehaviour {
    private h_Room inRoom;
    public float billyHappiness;
    public float satanistHappiness;
    public float monkHappiness;
    public float grannyHappiness;
    public MonoBehaviour[] activateOnPlace;
    private AudioSource audioSource;
    public AudioClip pickupSFX;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public h_Room InRoom
    {
        get
        {
            return inRoom;
        }
        set
        {
            inRoom = value;
            bool inside = true;
            if (value == null)
            {
                inside = false;
                //play audio
                if (audioSource != null && audioSource.isPlaying == false)
                {
                    audioSource.Play();
                }
            }
            /*else
            {
                if (audioSource != null)
                {
                    audioSource.Pause();
                }
            }*/
            foreach (var g in activateOnPlace)
            {
                g.enabled = inside;
            }
            gameObject.SendMessage("Inside");
        }
    }

    public float GetHappiness(h_Room.Owner owner) {
        switch (owner)
        {
            case h_Room.Owner.Billy:
                return billyHappiness;
            case h_Room.Owner.Satanist:
                return satanistHappiness;
            case h_Room.Owner.Monk:
                return monkHappiness;
            case h_Room.Owner.Granny:
                return grannyHappiness;
        }
        return 0f;
    }
}
