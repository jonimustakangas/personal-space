﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class h_Wind : MonoBehaviour {
    public float minVolDistance = 100f;
    float maxVolDistance = 500f;
    public float minVol = 0f;
    public float maxVol = 1f;
    AudioSource audioSource;
    //public Transform center;
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //maxVolDistance = Vector3.Distance(transform.position,center.position);
        maxVolDistance = DistanceToClosestRoom();
    }
    // Update is called once per frame
    void Update () {
        //float distance = Vector3.Distance(transform.position,center.position);
        float distance = DistanceToClosestRoom();
        float newVolume = Mathf.Lerp(minVol,maxVol,(distance-minVolDistance)/(maxVolDistance-minVolDistance));//inverse volume
        audioSource.volume = newVolume;
	}
    float DistanceToClosestRoom() {
        float distance = 9999f;
        foreach (var room in h_Room.rooms)
        {
            float newDistance = (room.transform.position - transform.position).magnitude;
            if (newDistance < distance)
            {
                distance = newDistance;
            }
        }
        return distance;
    }
}
