﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class h_LerpCamera : MonoBehaviour {
    Vector3 startPos;
    Quaternion startRot;
    public Transform[] targets;
    Camera cam;
    int current = 0;
    bool closeUp = false;
    public float moveSpeed = 5f;
    public float rotSpeed = 45f;
    // Use this for initialization
    void Start () {
        cam = Camera.main;
        startPos = cam.transform.position;
        startRot = cam.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.W))
        {
            closeUp = true;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            closeUp = false;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            current = Mathf.RoundToInt(Mathf.Clamp(current-1,0f,targets.Length-1));
            //Debug.Log(current);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            current = Mathf.RoundToInt(Mathf.Clamp(current+1, 0f, targets.Length-1));
            //Debug.Log(current);
        }
        Vector3 targetPos = startPos;
        Quaternion targetRot = startRot;
        if (closeUp == true)
        {
            targetPos = targets[current].position;
            targetRot = targets[current].rotation;
        }
        cam.transform.position = Vector3.MoveTowards(cam.transform.position,targetPos,moveSpeed * Time.deltaTime);
        cam.transform.rotation = Quaternion.RotateTowards(cam.transform.rotation,targetRot,rotSpeed * Time.deltaTime);
	}
}
