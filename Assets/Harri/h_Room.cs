﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class h_Room : MonoBehaviour {
    public static List<h_Room> rooms = new List<h_Room>();
    public GameObject[] itemsInside;
    public GameObject[] correctItems;
    public float happiness = 50;
    public UIFace face;
    public GameObject[] disableOnDeath;
    bool alive = true;
    public SpriteRenderer siluette;
    public Sprite[] siluetteSprites;
    public GameObject sacrificedGoat;
    public float Happiness {
        get { return happiness; }
        set {
            if (alive)
            {
                float dif = value - happiness;
                Debug.Log(owner.ToString() + " happiness " + happiness + " + " + dif + " = " + (happiness + dif));
                //happiness = Mathf.Clamp(value,0,100f);
                //happinesBar.value = happiness / 100f;
                happiness = value;
                happinesBar.value = Mathf.Clamp(value, 0, 100f) / 100f;
                //Debug.Log("resulting happiness " + happiness);
                int spriteIndex = Mathf.FloorToInt(happinesBar.value * (face.faces.Length - 1));
                face.ChangeFace(spriteIndex);
                siluette.sprite = siluetteSprites[spriteIndex];
                //change scene sprites too
                if (happiness <= 0)
                {
                    Die();
                }
            }
        }
    }
    public MeshCollider meshCollider;
    public enum Owner {
        Billy, Satanist, Monk, Granny
    }
    public Owner owner = Owner.Billy;
    public Slider happinesBar;
    // Use this for initialization
    void Awake() {
        meshCollider = GetComponent<MeshCollider>();
        rooms.Add(this);
    }
    private void Start()
    {
        Happiness = Happiness;
    }
    void Die() {
        Debug.Log(owner.ToString() + " ded");
        //enabled = false;
        foreach (var g in disableOnDeath)
        {
            g.SetActive(false);
        }
        alive = false;
        int index = 0;
        switch (owner)
        {
            case Owner.Billy:
                index = 0;
                break;
            case Owner.Satanist:
                index = 2;
                break;
            case Owner.Monk:
                index = 1;
                break;
            case Owner.Granny:
                index = 3;
                break;
        }
        AudioManager.instance.playDeathSound(index);
    }
    void CheckHappiness() {//unused spaghetti
        int correct = 0;
        foreach (var i in itemsInside)
        {
            bool isCorrect = false;
            foreach (var c in correctItems)
            {
                if (i == c)
                {
                    isCorrect = true;
                }
                //or check if gameObject == gameObject, name check kind of bad
                //maybe instantiate correct items and replace the array with instance references?

                /*if (i.name == c.name)
                {
                    Debug.Log(i.name + " oikeassa paikassa");
                    correct++;
                    break;
                }*/
            }
            if (isCorrect)
            {
                correct++;
            }
            else
            {
                correct--;
            }
        }
        //happiness = Mathf.Lerp(0f,100f,correct/correctItems.Length);
        happiness = (correctItems.Length / 2f + correct) / correctItems.Length;
    }
}
