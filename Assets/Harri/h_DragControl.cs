﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class h_DragControl : MonoBehaviour {
    public static h_DragControl instance;
    Camera cam;
    Collider dragged;
    Vector3 dragOffset = Vector3.zero;
    public float happinessMultiplier = 1f;
	// Use this for initialization
	void Start () {
        instance = this;
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        /*int layerMask = 1 << 8;
        layerMask = ~layerMask;//pasta bolognese*/

        Ray pointRay = cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(pointRay.origin,pointRay.direction * 100);
        RaycastHit hit = new RaycastHit();
        if (dragged != null)
        {
            if (Input.GetMouseButtonDown(0))
            {
                bool inside = false;
                /*foreach (var room in roomColliders)
                {
                    if (room.bounds.Contains(dragged.transform.position))
                    {
                        inside = true;
                    }
                }*/
                h_Room itemRoom = h_Room.rooms[0];//kakka init
                foreach (var room in h_Room.rooms)
                {
                    if (room.meshCollider.bounds.Contains(dragged.transform.position))
                    {
                        itemRoom = room;
                        inside = true;
                    }
                }
                if (inside)
                {
                    bool somethingInObject = false;
                    Collider[] inObject = Physics.OverlapBox(dragged.bounds.center, dragged.bounds.extents);
                    foreach (var c in inObject)
                    {
                        if (c.tag != "Prop" && c.gameObject.layer != 8)
                        {
                            somethingInObject = true;
                        }
                    }
                    if (somethingInObject == false)
                    {
                        Rigidbody rb = dragged.GetComponent<Rigidbody>();
                        if (rb != null)
                        {
                            rb.isKinematic = false;
                        }
                        h_Item item = dragged.GetComponent<h_Item>();
                        if (item != null)
                        {
                            item.InRoom = itemRoom;
                            itemRoom.Happiness += item.GetHappiness(itemRoom.owner) * happinessMultiplier;
                        }
                        //AudioManager.instance.AttachTo(cam.transform);
                        dragged = null;
                    }
                }
            }
            else
            {
                //dragged.transform.position = pointRay.origin + dragOffset;
                RaycastHit[] roomHits = Physics.RaycastAll(pointRay);
                dragged.transform.position = pointRay.origin + pointRay.direction * dragOffset.magnitude;
                foreach (var h in roomHits)
                {
                    if (h.collider.tag != "Prop" && h.collider.gameObject.layer != 8)
                    {
                        float groundOffset = dragged.transform.position.y - dragged.bounds.min.y + 0.2f;
                        dragged.transform.position = h.point + Vector3.up * groundOffset;
                    }
                }
            }
        }
        else
        {
            RaycastHit[] hits = Physics.RaycastAll(pointRay);
            foreach (var h in hits)
            {
                if (h.collider.tag == "Prop")
                {
                    hit = h;
                }
            }
            if (/*Physics.Raycast(pointRay, out hit) && */hit.collider != null && hit.collider.tag == "Prop")
            {
                Debug.DrawRay(hit.point, Vector3.up * 10, Color.red);
                if (Input.GetMouseButtonDown(0))
                {
                    dragged = hit.collider;
                    dragOffset = hit.transform.position - pointRay.origin;//Debug.Log("on");
                    Rigidbody rb = dragged.GetComponent<Rigidbody>();
                    if (rb != null)
                    {
                        rb.isKinematic = true;
                    }
                    h_Item item = dragged.GetComponent<h_Item>();
                    if (item != null)
                    {
                        if (item.InRoom != null)
                        {
                            item.InRoom.Happiness -= item.GetHappiness(item.InRoom.owner) * happinessMultiplier;
                            item.InRoom = null;
                        }
                    }
                    //AudioManager.instance.AttachTo(dragged.transform);
                }
            }
        }
	}
}
