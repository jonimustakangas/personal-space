﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour {

    public Texture2D[] cursorTextures;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    void Start()
    {
        Cursor.SetCursor(cursorTextures[0], hotSpot, cursorMode);
    }
    private void Update()
    {
        if (Input.GetMouseButton(0))
            Cursor.SetCursor(cursorTextures[1], hotSpot, cursorMode);
        else
            Cursor.SetCursor(cursorTextures[0], hotSpot, cursorMode);
    }
}
