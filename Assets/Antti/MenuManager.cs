﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    public GameObject help;
    public GameObject mainMenu;
    public GameObject ui;
    public GameObject credits;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            TogglePause();
    }
    private void Start()
    {
        ui.SetActive(false);
    }
    public void TogglePause()
    {
        if (mainMenu.activeInHierarchy) // Unpause
        {
            Time.timeScale = 1;
            mainMenu.SetActive(false);
            help.SetActive(false);
            ui.SetActive(true);
            // Deactivate Postprocessing blur effect 
        }
        else // Pause
        {
            Time.timeScale = 0;
            mainMenu.SetActive(true);
            ui.SetActive(false);
            // Activate Postprocessing blur effect 
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ToggleHelp()
    {
        help.SetActive(!help.activeInHierarchy);
    }
    public void ToggleCredits()
    {
        credits.SetActive(!credits.activeInHierarchy);
    }
}
