﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIFace : MonoBehaviour {

    public Sprite[] faces;
    Image sr;

	void Awake () {
        sr = GetComponent<Image>();
    }
	
    public void ChangeFace(int i)
    {
        sr.sprite = faces[i];
    }
}
