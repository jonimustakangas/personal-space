﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public static AudioManager instance = null;

    public Transform mCamera;
    Transform attachTo;
    AudioSource audioS;
    public AudioClip[] deaths;
    // Use this for initialization
    void Start () {
        instance = this;
        mCamera = Camera.main.transform;
        // Attach to main camera by default
        AttachTo(mCamera);
        audioS = GetComponent<AudioSource>();
    }
    public void playDeathSound(int index)
    {
        audioS.PlayOneShot(deaths[index]);
    }

	public void AttachTo(Transform target)
    {
        attachTo = target;
    }
    // Follow set target
    private void Update()
    {
        transform.position = attachTo.transform.position;
    }
    public void PlayAudio(AudioClip clip)
    {
        audioS.PlayOneShot(clip);
    }
}
