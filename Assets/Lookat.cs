﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lookat : MonoBehaviour {
    public  GameObject target;
    // Use this for initialization
    void Start () {

        

	}
	
	// Update is called once per frame
	void Update () {

        // Katsoo Targettia Y-rotaatiossa!!
        Vector3 targetposition = new Vector3(target.transform.position.x,transform.position.y, target.transform.position.z);
        transform.LookAt(targetposition);

    }
}
