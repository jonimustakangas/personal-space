﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class h_Altar : MonoBehaviour {
    public GameObject monkAltar;
    public GameObject satanistAltar;
    private void OnEnable()
    {
        h_Item item = GetComponent<h_Item>();
        if (item != null && item.InRoom != null)
        {
            switch (item.InRoom.owner)
            {
                case h_Room.Owner.Satanist:
                    satanistAltar.SetActive(true);
                    break;
                case h_Room.Owner.Monk:
                    monkAltar.SetActive(true);
                    break;
            }
        }
    }
    private void OnDisable()
    {
        monkAltar.SetActive(false);
        satanistAltar.SetActive(false);
    }

}
