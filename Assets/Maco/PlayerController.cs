﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 10.0f;
    public float rotationSpeed = 100.0f;
    public float horizontalSpeed = 2.0f;

    

  

    void Update()
    {
 
        float translation = Input.GetAxis("Vertical") * speed;
        float ad = Input.GetAxis("Horizontal") * speed;
        float rotation = Input.GetAxis("Mouse X") * horizontalSpeed;
        float rotationY = Input.GetAxis("Mouse Y") * horizontalSpeed;

        
        translation *= Time.deltaTime;
        ad *= Time.deltaTime;
        rotation *= Time.deltaTime;
        rotationY *= Time.deltaTime;

       
        transform.Translate(ad, 0, translation);

        if (Input.GetMouseButton(1))
        {
 
            transform.Rotate(0, rotation, 0);

        }

    }

}

